# Copyright 1999-2011 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=3

inherit cvs

DESCRIPTION="Tkpath implements path drawing modelled after its SVG counterpart"
HOMEPAGE="http://tclbitprint.sourceforge.net/"
SRC_URI=""

ECVS_SERVER="tclbitprint.cvs.sourceforge.net:/cvsroot/tclbitprint"
ECVS_MODULE="tkpath"
ECVS_USER="anonymous"
ECVS_PASS=""
ECVS_CVS_OPTIONS="-z3 -d"
ECVS_AUTH="pserver"

LICENSE="BSD"
SLOT="0"
KEYWORDS=""
IUSE="64bit threads"

DEPEND="
	dev-lang/tk
	x11-libs/cairo"
RDEPEND="${DEPEND}"

S="${WORKDIR}/${PN}"

src_prepare() {
	epatch "${FILESDIR}/man-pages.patch"
}

src_configure(){
	cd "${WORKDIR}/${PN}"
	econf \
		$(use_enable symbols) \
		$(use_enable 64bit) \
		$(use_enable threads)
}

src_compile() {
	emake || die
}

src_install() {
	emake DESTDIR="${D}" install || die
	dodoc ChangeLog doc/README.txt || die
	dodir /usr/share/${PN}/demos
	insinto /usr/share/${PN}/demos
	doins demos/*
}

