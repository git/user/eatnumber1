# Copyright 1999-2010 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI="2"
EGIT_REPO_URI="git://github.com/eatnumber1/${PN}.git"

inherit git autotools

DESCRIPTION="The Useless Programming Library"
HOMEPAGE="http://github.com/eatnumber1/libuseless"
SRC_URI=""

LICENSE="MIT"
SLOT="0"
KEYWORDS="~amd64"
IUSE=""

DEPEND="dev-lang/perl"
RDEPEND="${DEPEND}"

src_prepare() {
	eautoreconf || die
}

src_configure() {
	econf || die
}

src_install() {
	emake DESTDIR="${D}" install || die
}
