# Copyright 1999-2007 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2


inherit eutils java-pkg-2 java-ant-2

DESCRIPTION="Apache (not yet) commons-ssl library"
HOMEPAGE="http://http://juliusdavies.ca/commons-ssl"
SRC_URI="http://juliusdavies.ca/not-yet-commons-ssl-0.3.9/not-yet-${P}.zip"
EAPI=4
LICENSE="Apache-2.0"
SLOT="2"
KEYWORDS="amd64 ~ppc ~ppc64 x86 ~x86-fbsd"
IUSE="doc source samples"

RDEPEND=">=virtual/jre-1.4
    dev-java/commons-codec
    dev-java/commons-httpclient:3
    dev-java/commons-logging
    dev-java/log4j"

DEPEND=">=virtual/jdk-1.4
    ${RDEPEND}"

S=${WORKDIR}/not-yet-${P}

src_unpack() {
    unpack ${A}
    cd ${S}
    java-ant_rewrite-classpath build.xml
}

src_compile() {
    eant jar $(use doc javadocs)\
        -Dgentoo.classpath="$(java-pkg_getjars commons-codec):
	    $(java-pkg_getjars commons-logging):
	    $(java-pkg_getjars commons-httpclient-3):
	    $(java-pkg_getjars log4j)"
}

src_install() {
    java-pkg_dojar build/commons-ssl*.jar
    dodoc NOTICE.txt README.txt

    if use doc; then
	java-pkg_dojavadoc docs
	java-pkg_dohtml -r javadocs
    fi

    use source && java-pkg_dosrc src/*

    if use samples ; then
	dodoc -r samples
    fi
}