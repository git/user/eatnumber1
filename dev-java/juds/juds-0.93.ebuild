# Copyright 1999-2010 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI="2"

JAVA_PKG_IUSE="source doc"

inherit eutils java-pkg-2 toolchain-funcs

DESCRIPTION="Java Unix Domain Sockets (JUDS) provide classes to address the need
in Java for accessing Unix domain sockets."
HOMEPAGE="http://code.google.com/p/juds/"
SRC_URI="http://juds.googlecode.com/files/${P}.zip"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~amd64"

IUSE=""

COMMON_DEP=""

RDEPEND=">=virtual/jre-1.4
	${COMMON_DEP}"
DEPEND=">=virtual/jdk-1.4
	app-arch/unzip
	${COMMON_DEP}"

src_prepare() {
	epatch "$FILESDIR"/"${P}_gentoo.patch"
}

src_compile() {
	tc-export CC
	export JAVA_HOME="$(java-config -O)"
	emake || die
	if use doc; then
		emake javadoc || die
	fi
}

src_install() {
	java-pkg_dojar "${P}.jar"
	java-pkg_doso libunixdomainsocket.so
	use source && java-pkg_dosrc .
	use doc && java-pkg_dojavadoc doc
}

