# Copyright 1999-2010 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=2
EHG_REPO_URI="https://compcache.googlecode.com/hg/"
inherit eutils toolchain-funcs mercurial

DESCRIPTION="Compressed in-memory swap device for Linux"
HOMEPAGE="http://code.google.com/p/compcache/"
SRC_URI=""

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~amd64"
IUSE=""

S="${WORKDIR}/hg/sub-projects/rzscontrol"

src_prepare() {
	epatch "${FILESDIR}/${PN}-0.6_gentoo.patch"
}

src_compile() {
	tc-export CC
	default
}

src_install() {
	dobin rzscontrol || die
	doman man/rzscontrol.1 || die
}
